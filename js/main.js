$(function () {
  customSelectReplace()

  // --------------------------------------------------------------
  // MODALS
  const modal = $("#modal")
  let modalOpen = false
  let openModal

  $(".modal-trigger").click(function (e) {
    const target = $(this).data().target
    if ($("#" + target)[0]) {
      modal.fadeIn(100, function () {
        modalOpen = true
        $("#" + target).fadeIn(100)

        openModal = $("#" + target)[0]
      })
    }
  })
  //close modal
  $(document).click(function (event) {
    if (modalOpen && openModal) {
      if (
        (openModal.children[0] !== event.target &&
          $(event.target).parents(".book-service-form").length < 1 &&
          $(event.target).parents(".modal-content").length < 1 &&
          $(event.target).parents(".modal-item").length < 1 &&
          $(event.target).parents(".vue-modal-click").length < 1 &&
          $(event.target).parents(".calendar").length < 1 &&
          !openModal.children[0].contains(event.target) &&
          !$(event.target).hasClass("button-in-modal") &&
          !$(event.target).hasClass("component-button") &&
          !$(event.target).hasClass("component-button-icon") &&
          !$(event.target).hasClass("component-button-action")) ||
        $(".modal-close", openModal)[0] == event.target
      ) {
        // console.log(event.target.parentNode)
        $(openModal).fadeOut(100, function () {
          openModal = null
          componentId = null
          modal.fadeOut(100, function () {
            modalOpen = false
            if (typeof mainVue !== "undefined") {
              mainVue.reset()
            }
          })
        })
      }
    }
  })
  // ------------------------------------------------------------------
  /////////////////////////////////////////////////////////////////////////////////

  resizeElements()
  $(window).resize(function () {
    resizeElements()
  })

  if (document.querySelector(".fund-group")) {
    // Fund Basket Accordion for Single Fund Details
    $(".fund-group").each(function () {
      let listHeight = $("ul", this).height()

      if (!Array.from(this.classList).includes("active")) {
        $("ul", this).slideUp(0)
      }

      //adjust for scrollbar width
      if (listHeight < 400) {
        $("li", this).each(function () {
          $(this).css("grid-template-columns", "3fr 1fr 1fr 5rem")
        })
      }
      $(".fund-group-status", this).click(() => {
        if (Array.from(this.classList).includes("active")) {
          $("ul", this).slideUp("fast", () => {
            $(this).removeClass("active")
          })
        } else {
          $("ul", this).slideDown("fast", () => {
            $(this).addClass("active")
          })
        }
      })
    })
  }

  // Form Image Inputs
  if (document.querySelector(".upload-image")) {
    let formImageInputs = Array.from(document.querySelectorAll(".upload-image"))

    formImageInputs.forEach((formImage) => {
      const file = formImage.querySelector("input")
      const label = formImage.querySelector("label")
      const imageContainer = $(".image-control", label)[0]
      formImage.addEventListener("change", (e) => {
        const reader = new FileReader()
        reader.onload = function () {
          const img = new Image()
          img.src = reader.result
          imageContainer.removeChild(imageContainer.querySelector("img"))
          imageContainer.appendChild(img)
        }
        reader.readAsDataURL(file.files[0])
      })
    })
  }

  if (document.querySelector(".regions")) {
    $(".region").each(function () {
      let countries = $(".main-carousel", this).flickity({
        prevNextButtons: false,
      })
    })
  }

  // Colour Select Panels
  if (document.querySelector(".select-colour")) {
    $(".colour-panel").each(function () {
      $(this).css("background-color", `#${$(this).data("colour")}`)
    })
  }
})

function customSelectReplace() {
  var customSelectBox, i, j, l, selElmntLength, selElmnt, selected, items, itemContent
  /*look for any elements with the class "custom-select":*/
  customSelectBox = document.getElementsByClassName("custom-select")
  l = customSelectBox.length
  for (i = 0; i < l; i++) {
    selElmnt = customSelectBox[i].getElementsByTagName("select")[0]
    selElmntLength = selElmnt.length
    /*for each element, create a new DIV that will act as the selected item:*/
    selected = document.createElement("DIV")
    selected.setAttribute("class", "select-selected")
    selected.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML
    customSelectBox[i].appendChild(selected)
    /*for each element, create a new DIV that will contain the option list:*/
    items = document.createElement("DIV")
    items.setAttribute("class", "select-items select-hide")
    for (j = 1; j < selElmntLength; j++) {
      /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
      itemContent = document.createElement("DIV")
      itemContent.innerHTML = selElmnt.options[j].innerHTML
      itemContent.addEventListener("click", function (e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl
        s = this.parentNode.parentNode.getElementsByTagName("select")[0]
        sl = s.length
        h = this.parentNode.previousSibling
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i
            h.innerHTML = this.innerHTML
            y = this.parentNode.getElementsByClassName("same-as-selected")
            yl = y.length
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class")
            }
            this.setAttribute("class", "same-as-selected")
            break
          }
        }
        h.click()
      })
      items.appendChild(itemContent)
    }
    customSelectBox[i].appendChild(items)
    selected.addEventListener("click", function (e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation()
      closeAllSelect(this)
      this.nextSibling.classList.toggle("select-hide")
      this.classList.toggle("select-arrow-active")
    })
  }
  function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
  except the current select box:*/
    var x,
      y,
      i,
      xl,
      yl,
      arrNo = []
    x = document.getElementsByClassName("select-items")
    y = document.getElementsByClassName("select-selected")
    xl = x.length
    yl = y.length
    for (i = 0; i < yl; i++) {
      if (elmnt == y[i]) {
        arrNo.push(i)
      } else {
        y[i].classList.remove("select-arrow-active")
      }
    }
    for (i = 0; i < xl; i++) {
      if (arrNo.indexOf(i)) {
        x[i].classList.add("select-hide")
      }
    }
  }
  /*if the user clicks anywhere outside the select box,
then close all select boxes:*/
  document.addEventListener("click", closeAllSelect)
}

function resizeElements() {
  //Set Users List height to fill screen
  if (document.querySelector(".user-list-within")) {
    $("ul.users").height($(window).height() - $("header").height() - $(".users-list-header").height() - 120)

    let userDetailsInnerHeight = 0

    $(".user-details")
      .children()
      .each(function () {
        userDetailsInnerHeight += $(this).outerHeight()
      })
    if (userDetailsInnerHeight > $(".users-list-header").height() + $(".users").height()) {
      console.log("taller")
      console.log(userDetailsInnerHeight)
      $("ul.users").height(userDetailsInnerHeight - $(".users-list-header").height() - 40)
    }
  }

  // Set Product Notifications to fill screen
  if (document.querySelector(".product #notifications")) {
    console.log(window.innerHeight)
    console.log($("body").height() - window.innerHeight)
    $(".panel-body").each(function () {
      $(this).height($(".notification-panels").height() - ($("body").height() - window.innerHeight) - 120)
    })
    console.log($(".notification-panels").height())
  }
  //
}

// var availableTags = [
//   "ActionScript",
//   "AppleScript",
//   "Asp",
//   "BASIC",
//   "C",
//   "C++",
//   "Clojure",
//   "COBOL",
//   "ColdFusion",
//   "Erlang",
//   "Fortran",
//   "Groovy",
//   "Haskell",
//   "Java",
//   "JavaScript",
//   "Lisp",
//   "Perl",
//   "PHP",
//   "Python",
//   "Ruby",
//   "Scala",
//   "Scheme",
// ]
// function split(val) {
//   return val.split(/,\s*/)
// }
// function extractLast(term) {
//   return split(term).pop()
// }
// $("#tags")
//   // don't navigate away from the field on tab when selecting an item
//   .bind("keydown", function (event) {
//     if (event.keyCode === $.ui.keyCode.TAB && $(this).data("ui-autocomplete").menu.active) {
//       event.preventDefault()
//     }
//   })
//   .autocomplete({
//     minLength: 0,
//     source: function (request, response) {
//       // delegate back to autocomplete, but extract the last term
//       response($.ui.autocomplete.filter(availableTags, extractLast(request.term)))
//     },
//     focus: function () {
//       // prevent value inserted on focus
//       return false
//     },
//     select: function (event, ui) {
//       var terms = split(this.value)
//       // remove the current input
//       terms.pop()
//       // add the selected item
//       terms.push(ui.item.value)
//       // add placeholder to get the comma-and-space at the end
//       terms.push("")
//       this.value = terms.join(", ")
//       return false
//     },
//   })
